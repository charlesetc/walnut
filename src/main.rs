
extern crate pest;
#[macro_use]
extern crate pest_derive;

pub mod parser;
pub mod ast;
pub mod util;

use parser::convert;

fn main() {
    let source = "fn square x // hi there \n { x b c ; a b }";
    let asts = convert::unwrap(parser::items(source));
    for ast in asts {
        println!("{}", ast);
    }
}
