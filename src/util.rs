
pub fn join(strings: Vec<String>, sep: &str) -> String {
    let mut out = String::new();
    let mut first = true;
    for s in strings {
        if !first {
            out.push_str(sep);
        }
        out.push_str(&s);

        first = false;
    }
    out
}
