
use std::collections::HashMap;
use ast::{Ast, Meta, Span};
use super::{Error, Pair, Pairs, Rule};

pub fn asts(pairs: Pairs) -> Result<Vec<Ast<Meta>>, Vec<Error>> {
    let ast_results = pairs.map(|pair| pair_to_ast(pair)).collect::<Vec<
        Result<
            Ast<Meta>,
            Vec<Error>,
        >,
    >>();
    let mut asts = vec![];
    let mut errs = vec![];
    for r in ast_results {
        match r {
            Ok(ast) => asts.push(ast),
            Err(es) => errs.append(&mut es.clone()),
        }
    }
    if errs.len() == 0 { Ok(asts) } else { Err(errs) }
}

pub fn unwrap(r: Result<Vec<Ast<Meta>>, Vec<Error>>) -> Vec<Ast<Meta>> {
    match r {
        Err(errs) => {
            let mut displayed = String::new();
            for e in errs {
                displayed.push_str(&format!("{}", e));
                displayed.push_str("\n")
            }
            panic!("Error:\n{}", displayed)
        }
        Ok(asts) => asts,
    }
}

fn convert_int(span: Span) -> Ast<Meta> {
    let data = span.as_str().to_string();
    Ast::Integer {
        meta: Meta { span },
        data,
    }
}
fn convert_string(span: Span) -> Ast<Meta> {
    let data = span.as_str().to_string();
    Ast::String_ {
        meta: Meta { span },
        data,
    }
}
fn convert_ident(span: Span) -> Ast<Meta> {
    let data = span.as_str().to_string();
    Ast::Ident {
        meta: Meta { span },
        data,
    }
}
fn convert_apply(span: Span, children: Vec<Ast<Meta>>) -> Result<Ast<Meta>, Vec<Error>> {
    Ok(Ast::Call {
        meta: Meta { span },
        children,
    })
}

fn binary_operator(mut inner: Pairs, span: Span) -> Result<Ast<Meta>, Vec<Error>> {
    let left = inner.next().unwrap();
    let operator = inner.next().unwrap();
    let right = inner.next().unwrap();
    let name = operator.into_span().as_str().to_string();
    pair_to_ast(left).and_then(|left| {
        pair_to_ast(right).and_then(|right| {
            Ok(Ast::Call {
                meta: Meta { span: span.clone() },
                children: vec![
                    Ast::Access {
                        meta: Meta { span },
                        name: name,
                        child: Box::new(left),
                    },
                    right
                ]
            })
        })
    })
}

fn pair_to_ast(pair: Pair) -> Result<Ast<Meta>, Vec<Error>> {
    let rule = pair.as_rule();
    // Can definitely be optimized
    let mut inner = pair.clone().into_inner();
    let count = pair.clone().into_inner().count();
    let span = pair.into_span();
    match rule {
        Rule::expr
        | Rule::expr_nl
        | Rule::atom
        | Rule::operator
        | Rule::item =>
            // justification for unwrap:
            // in the grammar, these expressions just wrap an expr.
            pair_to_ast(inner.next().unwrap()),
        Rule::int => Ok(convert_int(span)),
        Rule::string => Ok(convert_string(span)),
        Rule::variant_start |
        Rule::ident => Ok(convert_ident(span)),
        Rule::product
        | Rule::product_nl =>
            if count == 1 {
                pair_to_ast(inner.next().unwrap())
            } else {
                binary_operator(inner, span)
            },
        Rule::sum
        | Rule::sum_nl =>
            if count == 1 {
                pair_to_ast(inner.next().unwrap())
            } else {
                binary_operator(inner, span)
            },
        Rule::update
        | Rule::update_nl =>
            if count == 1 {
                pair_to_ast(inner.next().unwrap())
            } else {
                panic!("unimplemented update")
            },
        Rule::let_expr =>
            if count == 1 {
                pair_to_ast(inner.next().unwrap())
            } else {
                let name = inner.next().unwrap().into_span().as_str().to_string();
                let expr = inner.next().unwrap();
                pair_to_ast(expr).and_then(|child| {
                    let child = Box::new(child);
                    Ok(Ast::Let { meta: Meta { span }, name, child })
                })
            },
        Rule::variant
        | Rule::variant_nl =>
            if count == 1 {
                pair_to_ast(inner.next().unwrap())
            } else {
                let name = inner.next().unwrap().into_span().as_str().to_string();
                asts(inner).and_then(|children|
                    Ok(Ast::Variant { meta: Meta { span }, name, children }))
            },
        Rule::access =>
            if count == 1 {
                pair_to_ast(inner.next().unwrap())
            } else {
                let child = inner.next().unwrap();
                let name = inner.next().unwrap().into_span().as_str().to_string();
                pair_to_ast(child).and_then(|child| {
                    Ok(Ast::Access { meta: Meta { span }, name, child: Box::new(child)})
                })
            },
        Rule::apply_nl
        | Rule::apply => {
            if count == 1 {
                pair_to_ast(inner.next().unwrap())
            } else {
                asts(inner).and_then(|children| convert_apply(span, children))
            }
        }
        Rule::object => {
            if count == 1 {
                pair_to_ast(inner.next().unwrap())
            } else {
                let mut fields = Ok(HashMap::<String, Ast<Meta>>::new());
                while let Some(name) = inner.next() {
                    let name = name.into_span().as_str().to_string();
                    let child = inner.next().unwrap();
                    fields = fields.and_then(|mut fields|
                        pair_to_ast(child).and_then(|ast| {
                            fields.insert(name, ast);
                            Ok(fields)
                        }));
                }
                fields.and_then(|fields| {
                    Ok(Ast::Object { meta: Meta { span }, fields })
                })
            }
        }
        Rule::lambda => {
            // functions will always have a name and
            let mut args = vec![];
            loop {
                // the way it's parsed, there will
                // always be a begin{} before the end.
                let arg = inner.next().unwrap();
                if arg.as_rule() == Rule::begin {
                    break;
                } else {
                    args.push(arg.into_span().as_str().to_string())
                }
            }
            asts(inner).and_then(|children|
                Ok(Ast::Lambda { meta: Meta { span }, args, children }))
        }
        Rule::function => {
            // functions will always have a name and
            let name = inner.next().unwrap().into_span().as_str().to_string();
            let mut args = vec![];
            loop {
                // the way it's parsed, there will
                // always be a begin{} before the end.
                let arg = inner.next().unwrap();
                if arg.as_rule() == Rule::begin {
                    break;
                } else {
                    args.push(arg.into_span().as_str().to_string())
                }
            }
            asts(inner).and_then(|children|
                Ok(Ast::Function { meta: Meta { span }, name, args, children }))
        }
        _ => { panic!("unimplemented: {:?} {}", rule, span.as_str()) }
    }
}

#[cfg(test)]
mod tests {

    use parser;
    use ast;
    use super::*;

    fn tree(source: &str) -> Result<String, Vec<Error>> {
        let pairs = parser::parse_expr(source).unwrap_or_else(|e| panic!("{}", e));
        let asts = asts(pairs);
        asts.and_then(|trees| Ok(ast::join_asts(&trees, "\n")))
    }

    fn assert_tree(left: &str, right: &str) {
        assert_eq!(tree(left), Ok(right.to_string()));
        assert_eq!(tree(right), Ok(right.to_string()));
    }

    #[test]
    fn test_call() {
        assert_tree("f a b", "(f a b)");
        assert_tree("f (a c) b", "(f (a c) b)");
    }

    #[test]
    fn test_lambda() {
        assert_tree("{ }", "{  }");
        assert_tree("{ hi }", "{ hi }");
        assert_tree(": a b { hi }", ": a b { hi }");
    }

    #[test]
    fn test_variant() {
        assert_tree("Hi you 2", "(Hi you 2)");
        assert_tree("Hi there (Cool you)", "(Hi there (Cool you))");
    }

    #[test]
    fn test_operator() {
        assert_tree("2 + 2", "(2.+ 2)");
        assert_tree("(2\n+2)", "(2.+ 2)");
        assert_tree("2 +//hi\n2", "(2.+ 2)");
        assert_tree("2 + 2 * 3", "(2.+ (2.* 3))");
    }

    #[test]
    fn test_let() {
        assert_tree("{let x = 3}", "{ let x = 3 }");
        assert_tree("{let x = 3 ; x}", "{ let x = 3 ; x }");
    }

    #[test]
    fn test_object() {
        assert_tree("{this: 2}", "{this: 2}");
        assert_tree("{this: 4, that: hi}", "{that: hi, this: 4}");
        assert_tree("{this: {that: 5}}", "{this: {that: 5}}");
    }

}
