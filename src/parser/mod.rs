
use pest;
use ast::{Ast, Meta};
use pest::Parser;

pub mod convert;

// relative to this file
const _PEST: &'static str = include_str!("../walnut.pest");

#[derive(Parser)]
#[grammar = "walnut.pest"]
struct Expr;

type Pair = pest::iterators::Pair<Rule, pest::inputs::StringInput>;
type Pairs = pest::iterators::Pairs<Rule, pest::inputs::StringInput>;
type Error = pest::Error<Rule, pest::inputs::StringInput>;

pub fn parse_item(source: &str) -> Result<Pairs, Error> {
    Expr::parse_str(Rule::item, source)
}
pub fn parse_expr(source: &str) -> Result<Pairs, Error> {
    Expr::parse_str(Rule::expr, source)
}

pub fn items(source: &str) -> Result<Vec<Ast<Meta>>, Vec<Error>> {
    use convert;
    let pairs = parse_item(source)
        .unwrap_or_else(|e| panic!("{}", e));
    convert::asts(pairs)
}

#[cfg(test)]
mod tests {

    use super::*;

    fn readable_pair(pair: Pair) -> String {
        let mut out = String::new();
        match pair.as_rule() {
            Rule::int
                | Rule::ident
                | Rule::string
                | Rule::variant_start => {
                    out.push_str(&format!("{}", pair.into_span().as_str()));
                },
            _ => {
                if pair.clone().into_inner().count() == 1 {
                    out.push_str(&readable_pair(pair.into_inner().last().unwrap()));
                } else {
                    out.push_str(&format!("{:?}", pair.as_rule()));
                    out.push_str("{");
                    let mut first = true;
                    for pair in pair.clone().into_inner() {
                        if !first { out.push_str(", "); }
                        out.push_str(&readable_pair(pair));
                        first = false;
                    }
                    out.push_str("}");
                }
            }
        }
        out
    }

    fn rparse_i(source: &str) -> Result<String, Error> {
        parse_item(source).map(|parsed| {
            let mut out = String::new();
            for pair in parsed {
                out.push_str(&readable_pair(pair));
            }
            out
        })
    }

    fn rparse_e(source: &str) -> Result<String, Error> {
        parse_expr(source).map(|parsed| {
            let mut out = String::new();
            for pair in parsed {
                out.push_str(&readable_pair(pair));
            }
            out
        })
    }

    #[test]
    fn test_ident() {
        assert_eq!(rparse_e("hi"), Ok("hi".to_string()));
        assert_eq!(rparse_e("h2"), Ok("h2".to_string()));
    }

    #[test]
    fn test_operator() {
        assert_eq!(rparse_e("2 + 2 + 3"), Ok("sum{2, plus{}, sum{2, plus{}, 3}}".to_string()));
        assert_eq!(rparse_e("2 + 2 / 3"), Ok("sum{2, plus{}, product{2, divide{}, 3}}".to_string()));
        assert_eq!(rparse_e("2 + (2 / 3)"), Ok("sum{2, plus{}, product_nl{2, divide{}, 3}}".to_string()));
        assert_eq!(rparse_e("(2 + 2) / 3"), Ok("product{sum_nl{2, plus{}, 2}, divide{}, 3}".to_string()));
        assert_eq!(rparse_e("2 / 2 + 3"), Ok("sum{product{2, divide{}, 2}, plus{}, 3}".to_string()));
    }

    #[test]
    fn test_apply() {
        assert_eq!(rparse_e("f a"), Ok("apply{f, a}".to_string()));
        assert_eq!(rparse_e("(f\na\n/\n3)"), Ok("product_nl{apply_nl{f, a}, divide{}, 3}".to_string()));
    }

    #[test]
    fn test_newline() {
        // this one is not in parentheses, like the rest:Y
        assert_eq!(rparse_e("22 +\n3"), Ok("sum{22, plus{}, 3}".to_string()));
        assert_eq!(rparse_e("(22\n+ 3)"), Ok("sum_nl{22, plus{}, 3}".to_string()));
        assert_eq!(rparse_e("(22\n+\n3)"), Ok("sum_nl{22, plus{}, 3}".to_string()));
        assert_eq!(rparse_e("(\n22\n+\n3\n)"), Ok("sum_nl{22, plus{}, 3}".to_string()));
        assert_eq!(rparse_e("(\n22+3\n)"), Ok("sum_nl{22, plus{}, 3}".to_string()));
        let this_match = Ok("sum_nl{22, plus{}, product_nl{3, times{}, 3}}".to_string());
        assert_eq!(rparse_e("(\n22+3\n*3)"), this_match);
        assert!(rparse_e("22+3\n*3") != this_match);
        assert_eq!(rparse_e("(f\na)"), Ok("apply_nl{f, a}".to_string()));
        // of note: this isn't an apply
        assert_eq!(rparse_e("{f\n a}"), Ok("lambda{begin{}, f, a}".to_string()));
        assert_eq!(rparse_e("{(f\n\na)}"), Ok("lambda{begin{}, apply_nl{f, a}}".to_string()));
        assert_eq!(rparse_e("(\nf a\n)"), Ok("apply_nl{f, a}".to_string()));
    }

    #[test]
    fn test_lambda() {
        assert_eq!(rparse_e("{}"), Ok("begin{}".to_string()));
        assert_eq!(rparse_e(": a b { }"), Ok("lambda{a, b, begin{}}".to_string()));
        assert_eq!(rparse_e(": a b { c }"), Ok("lambda{a, b, begin{}, c}".to_string()));
        assert_eq!(rparse_e(": { c }"), Ok("lambda{begin{}, c}".to_string()));
        assert_eq!(rparse_e("{ c }"), Ok("lambda{begin{}, c}".to_string()));
        assert_eq!(rparse_e("{ c ; d; e }"), Ok("lambda{begin{}, c, d, e}".to_string()));
        assert_eq!(rparse_e("f : x { c }"), Ok("apply{f, lambda{x, begin{}, c}}".to_string()));
        assert_eq!(rparse_e(": x { c } + f"), Ok("sum{lambda{x, begin{}, c}, plus{}, f}".to_string()));
        assert_eq!(rparse_e(": x { a b ; c d }"), Ok("lambda{x, begin{}, apply{a, b}, apply{c, d}}".to_string()));
        assert_eq!(rparse_e(": x { a b \n c d }"), Ok("lambda{x, begin{}, apply{a, b}, apply{c, d}}".to_string()));
    }

    #[test]
    fn test_object() {
        assert_eq!(rparse_e("{eyes: 2}"), Ok("object{eyes, 2}".to_string()));
        assert_eq!(rparse_e("{eyes: 2, legs: square 2}"), Ok("object{eyes, 2, legs, apply_nl{square, 2}}".to_string()));
        assert_eq!(rparse_e("{ eyes : f { x } }"), Ok("lambda{begin{}, apply{eyes, lambda{f, begin{}, x}}}".to_string()));
        assert_eq!(rparse_e("{ eyes: f { x } }"), Ok("object{eyes, apply_nl{f, lambda{begin{}, x}}}".to_string()));
        assert_eq!(rparse_e("{\neyes:\n\n2,\nlegs: square\n2\n}"), Ok("object{eyes, 2, legs, apply_nl{square, 2}}".to_string()));
        assert_eq!(rparse_e("{eyes: {legs: 4}}"), Ok("object{eyes, object{legs, 4}}".to_string()));
    }

    #[test]
    fn test_function() {
        assert_eq!(rparse_i("fn square x {x * x}"), Ok("function{square, x, begin{}, product{x, times{}, x}}".to_string()));
        assert_eq!(rparse_i("fn square x {f\na}"), Ok("function{square, x, begin{}, f, a}".to_string()));
        assert_eq!(rparse_i("fn square x {(f\na)}"), Ok("function{square, x, begin{}, apply_nl{f, a}}".to_string()));
        assert_eq!(rparse_i("fn square x a b c { a }"), Ok("function{square, x, a, b, c, begin{}, a}".to_string()));
        assert_eq!(rparse_i("fn\nsquare\nx a\nb c\n{ a }"), Ok("function{square, x, a, b, c, begin{}, a}".to_string()));
        // operators know when they can go to the next line
        assert_eq!(rparse_i("fn square x {x *\nx}"), Ok("function{square, x, begin{}, product{x, times{}, x}}".to_string()));
        assert_eq!(rparse_i("fn square x\n{\nx *x\n}"), Ok("function{square, x, begin{}, product{x, times{}, x}}".to_string()));
    }

    #[test]
    fn test_field_access() {
        assert_eq!(rparse_e("one.two"), Ok("access{one, two}".to_string()));
        assert_eq!(rparse_e("one.two.three"), Ok("access{one, two, three}".to_string()));
        assert_eq!(rparse_e("(one two).three"), Ok("access{apply_nl{one, two}, three}".to_string()));
        assert_eq!(rparse_e("{zero\none\n.two\n.three}"), Ok("lambda{begin{}, zero, access{one, two, three}}".to_string()));
        // the lambda isn't printed because it only has one argument - begin
        assert_eq!(rparse_e("{x}.two"), Ok("access{lambda{begin{}, x}, two}".to_string()));
    }

    #[test]
    fn test_let() {
        assert_eq!(rparse_i("fn main { let x = 3 ; x }"), Ok("function{main, begin{}, let_expr{x, 3}, x}".to_string()));
        assert_eq!(rparse_e("{let x = 3}"), Ok("lambda{begin{}, let_expr{x, 3}}".to_string()));
        assert_eq!(rparse_e("{let x = 3 + 2}"), Ok("lambda{begin{}, let_expr{x, sum{3, plus{}, 2}}}".to_string()));
        assert_eq!(rparse_e("{\nlet\nx\n=\n3\n}"), Ok("lambda{begin{}, let_expr{x, 3}}".to_string()));
    }

    #[test]
    fn test_update() {
        assert_eq!(rparse_i("fn main { x = 3 } "), Ok("function{main, begin{}, update{x, 3}}".to_string()));
        assert_eq!(rparse_e("{x = 3 * 3}"), Ok("lambda{begin{}, update{x, product{3, times{}, 3}}}".to_string()));
        assert_eq!(rparse_e("{x =\n3}"), Ok("lambda{begin{}, update{x, 3}}".to_string()));
        assert_eq!(rparse_e("{let x = y = 3}"), Ok("lambda{begin{}, let_expr{x, update{y, 3}}}".to_string()));
        assert!(rparse_e("{x\n= 3}").is_err());
        assert!(rparse_e("(x\n= 3)").is_ok());
    }

    #[test]
    fn test_variant() {
        assert_eq!(rparse_e("(Some 2)"), Ok("variant_nl{Some, 2}".to_string()));
        assert_eq!(rparse_e("S ome"), Ok("variant{S, ome}".to_string()));
        assert_eq!(rparse_e("{ Apple 2 5.this }"), Ok("lambda{begin{}, variant{Apple, 2, access{5, this}}}".to_string()));
        assert_eq!(rparse_e("(\nSome\n2\n3\n)"), Ok("variant_nl{Some, 2, 3}".to_string()));
        assert_eq!(rparse_e("Some 2 * 2"), Ok("product{variant{Some, 2}, times{}, 2}".to_string()));
    }

    #[test]
    fn test_string() {
        assert_eq!(rparse_e("\"\""), Ok("\"\"".to_string()));
        assert_eq!(rparse_e("\"a\""), Ok("\"a\"".to_string()));
        assert_eq!(rparse_e("\"\\ta\""), Ok("\"\\ta\"".to_string()));
        assert_eq!(rparse_e("f a \"a\" b"), Ok("apply{f, a, \"a\", b}".to_string()));
        assert_eq!(rparse_e("'hi there'"), Ok("'hi there'".to_string()));
    }

    #[test]
    fn test_comment() {
        assert_eq!(rparse_e("x // hi there\n"), Ok("x".to_string()));
        assert_eq!(rparse_i("fn main //hi you\n {let // hi \nx = 3 //hi there \n x }"),
            Ok("function{main, begin{}, let_expr{x, 3}, x}".to_string()));
    }

    #[test]
    fn test_match() {
        assert_eq!(rparse_e("match a {| x -> x}"), Ok("match_{a, branch{x, begin{}, x}}".to_string()));
        assert_eq!(rparse_e("match a {| x | y | z -> x}"), Ok("match_{a, branch{x, y, z, begin{}, x}}".to_string()));
        assert_eq!(rparse_e("match a {| Three | One True (Three b) -> x}"),
            Ok("match_{a, branch{Three, match_expr{One, match_expr{True, match_expr{Three, b}}}, begin{}, x}}".to_string()));
        assert_eq!(rparse_e("match \n a \n{\n| Three \n| One \n True (Three b\n) -> x}"),
            Ok("match_{a, branch{Three, match_expr{One, match_expr{True, match_expr{Three, b}}}, begin{}, x}}".to_string()));
        assert_eq!(rparse_e("match a {| x ->\nlet x = 3\nx}"), Ok("match_{a, branch{x, begin{}, let_expr{x, 3}, x}}".to_string()));
        assert_eq!(rparse_e("match a {| x -> match x {|y -> y}}"), Ok("match_{a, branch{x, begin{}, match_{x, branch{y, begin{}, y}}}}".to_string()));
    }

}
