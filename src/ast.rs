
use std::collections::HashMap;
use std::fmt;
use pest;
use util;

pub type Span = pest::inputs::Span<pest::inputs::StringInput>;

#[derive(Debug)]
pub struct Meta {
    pub span: Span,
    // pub start: usize,
    // pub end: usize,
}

pub fn join_asts<T>(asts: &Vec<Ast<T>>, sep: &str) -> String {
    util::join(asts.iter().map(|a| a.to_string()).collect::<Vec<_>>(), sep)
}

use self::Ast::*;
impl<T> Ast<T> {

    fn as_string(&self) -> String {
        match self {
            &Let { meta: _, ref name, ref child } =>
                // not in parents because it's only parsed within blocks.
                format!("let {} = {}", name, child),
            &Lambda { meta: _, ref args, ref children } =>
                if args.len() == 0 {
                    format!("{{ {} }}",
                            join_asts(children, " ; "))
                } else {
                    format!(": {} {{ {} }}",
                            util::join(args.to_vec(), " "),
                            join_asts(children, " ; "))
                },
            &Function { meta: _, ref name, ref args, ref children } =>
                format!("fn {} {} {{ {} }}",
                        name,
                        util::join(args.to_vec(), " "),
                        join_asts(children, " ; ")),
            &Integer { meta: _, ref data } | &Ident { meta: _, ref data } => data.to_string(),
            &String_ { meta: _, ref data } => "\"".to_string() + &data + "\"",
            &Object { meta: _, ref fields } => {
                let mut out = String::new();
                let mut first = true;
                out.push_str("{");

                // This part is needed to ensure that we get a deterministic
                // string. Hash.iter() doesn't have a deterministic order.
                let mut field_vec = fields.iter().collect::<Vec<_>>();
                field_vec.sort_by(|a, b| a.0.cmp(b.0));

                for &(name, ast) in field_vec.iter() {
                    if !first { out.push_str(", ") }
                    first = false;
                    out.push_str("");
                    out.push_str(name);
                    out.push_str(" ");
                    out.push_str(&ast.to_string());
                }
                out + "}"
            }
            &Access { meta: _, ref name, ref child } => child.as_string() + "." + name,
            &Call { meta: _, ref children } =>
                "(".to_string() +
                &join_asts(children, " ")
                + ")",
            &Variant { meta: _, ref name, ref children } => "(".to_string() +
                name + " " +
                &join_asts(children, " ")
                + ")",
            _ => unimplemented!()
        }
    }

}

impl<T> fmt::Display for Ast<T> {

    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.as_string())
    }

}

#[derive(Debug)]
pub enum Ast<T> {
    Function {
        meta: T,
        name: String,
        args: Vec<String>,
        children: Vec<Ast<T>>
    },

    Lambda {
        meta: T,
        args: Vec<String>,
        children: Vec<Ast<T>>
    },

    Call {
        meta: T,
        children: Vec<Ast<T>>
    },

    Object {
        meta: T,
        fields: HashMap<String, Ast<T>>
    },

    Access {
        meta: T,
        name: String,
        child: Box<Ast<T>>,
    },

    Let {
        meta: T,
        name: String,
        child: Box<Ast<T>>
    },

    Update {
        meta: T,
        left: Box<Ast<T>>,
        right: Box<Ast<T>>
    },

    If {
        meta: T,
        cond: Box<Ast<T>>,
        truebranch: Vec<Ast<T>>,
        elsebranch: Vec<Ast<T>>
    },

    Variant {
        meta: T,
        name: String,
        children: Vec<Ast<T>>
    },

    Match {
        meta: T,
        branches: Vec<(Ast<T>, Vec<Ast<T>>)>
    },

    Integer {
        meta: T,
        data: String,
    },

    String_ {
        meta: T,
        data: String,
    },

    Ident {
        meta: T,
        data: String,
    },
}
